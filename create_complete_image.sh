rm -rf initramfs_busybox
mkdir initramfs_busybox
cd initramfs_busybox
cp -r ../busybox-1.36.1/_install/* .
rm linuxrc
ln -s bin/sh init
mkdir proc sys tmp root var mnt dev etc
sudo mknod dev/console c 5 1
sudo mknod dev/tty1 c 4 1
sudo mknod dev/tty2 c 4 2
sudo mknod dev/tty3 c 4 3
sudo mknod dev/tty4 c 4 4
cd etc
# Le fichier etc/inittab est tiré directement de l’exemple fourni par BusyBox.
# Il lui indique ce qu’il doit exécuter au démarrage et à l’arrêt du système et les terminaux à ouvrir.
cp ../../busybox-1.36.1/examples/inittab .
mkdir init.d
cd init.d
# Le fichier etc/init.d/rcS est un script exécuté par BusyBox au démarrage.
# Tel que rempli ci-dessus, il monte les systèmes de fichiers décrits dans /etc/fstab,
# active mdev qui va peupler statiquement et dynamiquement /dev et configure
# l’interface réseau loopback.
cat <<EOF > rcS
#!/bin/sh
mount -a
mkdir -p /dev/pts
mount -t devpts devpts /dev/pts
echo /sbin/mdev > /proc/sys/kernel/hotplug
mdev -s
mkdir -p /var/lock
ifconfig lo 127.0.0.1
EOF
chmod 755 rcS
cd ..
cat <<EOF > fstab
proc             /proc           proc    defaults                0       0
tmpfs            /tmp            tmpfs   defaults                0       0
sysfs            /sys            sysfs   defaults                0       0
tmpfs            /dev            tmpfs   defaults                0       0
EOF
cd ..
find . | cpio -o -H newc -R +0:+0 | gzip > initramfs.gz
cd ..
# ./qemu-system-arm -machine vexpress-a9 -nographic -kernel \
# linux-5.15.6/build/arch/arm/boot/zImage \
# -dtb linux-5.15.6/build/arch/arm/boot/dts/vexpress-v2p-ca9.dtb \
# -initrd initramfs_busybox/initramfs.gz
