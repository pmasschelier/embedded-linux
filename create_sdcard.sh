# Nettoyage de toute éventuelle ancienne image
rm -rf sdcard
mkdir sdcard
cd sdcard
# Création d'un fichier sd composé de 1Mo de zéros.
dd if=/dev/zero of=sd bs=1M count=64
# Création d'une partition sur sd
cat <<EOF | parted sd
mklabel msdos
mkpart primary fat32 1MiB 100%
print
quit
EOF
# Device devrait être /dev/loop0
DEVICE=`sudo losetup -f --show -P sd`
echo "Device: $DEVICE"
# Création d'un système de fichier Fat32 (similaire à ceux généralement utilisés sur les cartes SD)
sudo mkfs.fat -F 32 ${DEVICE}p1
# monte le système de fichier nouvellement créé sur mnt
mkdir mnt
sudo mount ${DEVICE}p1 mnt
# Copie les images nécessaire sur le disque
sudo cp ../linux-5.15.6/build/arch/arm/boot/zImage mnt
sudo cp ../linux-5.15.6/build/arch/arm/boot/dts/vexpress-v2p-ca9.dtb mnt
sudo cp ../initramfs_busybox/initramfs.gz mnt
cd mnt
# Ajoute un en-tête U-Boot à l'image mémoire initiale (initramfs)
sudo ../../u-boot-2020.10/tools/mkimage -A arm -O linux -T ramdisk -d initramfs.gz uinitramfs
cd ..
sudo umount mnt
sudo losetup -d ${DEVICE}$
cd ..
